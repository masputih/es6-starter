module.exports = {
  port: 8000,
  open: false,
  files: ['./public/**/*.{html,css,js}'],
  server: {
    baseDir: './public'
  }
};
