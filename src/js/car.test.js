import Car from './car';

test('spare tire is one, front & back are 2', () => {
  expect(Car.getTires('spare')).toBe(1);
  expect(Car.getTires('front')).toBe(2);
  expect(Car.getTires('back')).toBe(2);
});
