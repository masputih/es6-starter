import '../styles/styles.scss';

import Car from './car';

const s = 'Hello es6';
console.log(s);

const myobj = { home: 'aaa', away: 'bbb' };
const { home } = myobj;
const copyobj = {...myobj};

console.log('new home', home);

// TODO: fix this

var myArray = [
  1, 2, 3,
  4, 5, 6
];

let xy = 2;
let zi = 3;

const myArrOfObj = [
  {
    aaa: 1,
    bbb: 2
  }
];

/**
 * @param {number} arg1 - Deskripsi parameter.
 */
function myfn (arg1) {
  myArray.pop();
  myArray.push(xy, zi);
  debugger;
  myArray.push(myArrOfObj);
  myArray.shift(copyobj);
}
myfn();

var myfn2 = ({ name } = {}) => {
  myArray.unshift(name);
  var car = Car;

  if (car.brand === 'toyota') {
    myArray.pop();
  }

  myArray.push(car);

  let newArray = [...myArray];
  newArray.push(car);

  car.drive();
};

myfn2({
  name: 'aa', home: 'bb'
});
